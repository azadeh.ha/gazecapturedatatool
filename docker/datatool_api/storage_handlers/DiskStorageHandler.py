import json
import os
import shutil
from ..data_validation.base_model import BaseModel
from ..interfaces.StorageIOInterface import StoragePointer, StorageHandlerInterface


class DiskStoragePointer(StoragePointer):
	__slots__ = ["path"]
	
	def __init__(self, object_id, class_type, pointer_path):
		super().__init__(object_id, class_type)
		self.path = pointer_path


class DiskStorageHandler(StorageHandlerInterface):
	def __init__(self, root_path: str):
		self.root_path = root_path
	
	def write(self, object_id, data_category: str, data_object: BaseModel) -> DiskStoragePointer:
		object_path = os.path.join(self.root_path, data_category, object_id)
		os.makedirs(os.path.dirname(object_path), exist_ok=True)
		with open(object_path, "w") as r:
			json.dump(data_object.dict(), r)
		sample = DiskStoragePointer(object_id, type(data_object), object_path)
		return sample
	
	def read(self, pointer: DiskStoragePointer, remove=False) -> BaseModel:
		with open(pointer.path, "r") as r:
			data = json.load(r)
			out_object = pointer.type().from_dict(data)
			if remove is True:
				os.remove(pointer.path)
			return out_object
	
	def close(self):
		if os.path.exists(self.root_path):
			shutil.rmtree(self.root_path)

	def remove(self, pointer: DiskStoragePointer) -> None:
		os.remove(pointer.path)
