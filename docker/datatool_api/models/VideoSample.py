from .base.BaseSample import BaseSample
from .VideoInterval import VideoInterval
from ..data_validation.attribute import Attribute
from .BaseTypes import VideoSampleMetadata
from ..data_validation.containers.ValDict import ValDict


class VideoSample(BaseSample):
    intervals: ValDict[str, VideoInterval] = Attribute(type=ValDict, min_items=1,
                                                       element_constraints=Attribute(
                                                           type=VideoInterval,
                                                           rsa=['id', 'index', 'startFrame', 'endFrame'],
                                                           osa=['subjects', 'objects'])
                                                       )
    metadata: VideoSampleMetadata = Attribute(type=VideoSampleMetadata, msa=1)

    def __init__(self, id: str = None, samplePath: str = None, intervals: ValDict[str, VideoInterval] = None,
                 metadata: VideoSampleMetadata = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)
