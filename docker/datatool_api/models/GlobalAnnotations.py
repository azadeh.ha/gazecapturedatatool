from .Categories import *
from .BaseTypes import *


class GlobalAnnotations(BaseModel):
    eyes: GlobalEyes = Attribute(type=GlobalEyes, msa=1)
    face: GlobalFace = Attribute(type=GlobalFace, msa=1)
    body: GlobalBody = Attribute(type=GlobalBody, msa=1)
    hands: GlobalHands = Attribute(type=GlobalHands, msa=1)
    feet: GlobalFeet = Attribute(type=GlobalFeet, msa=1)

    def __init__(self, eyes: GlobalEyes = None, face: GlobalFace = None, body: GlobalBody = None,
                 hands: GlobalHands = None, feet: GlobalFeet = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    @staticmethod
    def _distinguish_label_place(annotation, label):
        mapping = {
            GlobalEye: lambda x: '.eyes.{}'.format(x),
            GlobalHand: lambda x: '.hands.{}'.format(x),
            GlobalFoot: lambda x: '.feet.{}'.format(x),
            LandmarkLabel: lambda x: {'face': {Categories.LandmarkType.L2d: '.face.landmarkMap2d',
                                               Categories.LandmarkType.L3d: '.face.landmarkMap3d'
                                               },
                                      'body': {Categories.LandmarkType.L2d: '.body.landmarkMap2d',
                                               Categories.LandmarkType.L3d: '.body.landmarkMap3d'
                                               },
                                      'leftEye': {Categories.LandmarkType.L2d: '.eyes.leftEye.landmarkMap2d',
                                                  Categories.LandmarkType.L3d: '.eyes.leftEye.landmarkMap3d'
                                                  },
                                      'rightEye': {Categories.LandmarkType.L2d: '.eyes.rightEye.landmarkMap2d',
                                                   Categories.LandmarkType.L3d: '.eye.rightEye.landmarkMap3d'
                                                   },
                                      'leftHand': {Categories.LandmarkType.L2d: '.hands.leftHand.landmarkMap2d',
                                                   Categories.LandmarkType.L3d: '.hands.leftHand.landmarkMap3d'
                                                   },
                                      'rightHand': {Categories.LandmarkType.L2d: '.hands.rightHand.landmarkMap2d',
                                                    Categories.LandmarkType.L3d: '.hands.rightHand.landmarkMap3d'
                                                    },
                                      'leftFoot': {Categories.LandmarkType.L2d: '.feet.leftFoot.landmarkMap2d',
                                                   Categories.LandmarkType.L3d: '.feet.leftFoot.landmarkMap3d'
                                                   },
                                      'rightFoot': {Categories.LandmarkType.L2d: '.feet.rightFoot.landmarkMap2d',
                                                    Categories.LandmarkType.L3d: '.feet.rightFoot.landmarkMap3d'
                                                    },
                                      }.get(x),
            SkeletonEdge: lambda x: {'face': {Categories.SkeletonType.S2d: '.face.skeleton2d',
                                              Categories.SkeletonType.S3d: '.face.skeleton3d'
                                              },
                                     'body': {Categories.SkeletonType.S2d: '.body.skeleton2d',
                                              Categories.SkeletonType.S3d: '.body.skeleton3d'
                                              },
                                     'leftEye': {Categories.SkeletonType.S2d: '.eyes.leftEye.skeleton2d',
                                                 Categories.SkeletonType.S3d: '.eyes.leftEye.skeleton3d'
                                                 },
                                     'rightEye': {Categories.SkeletonType.S2d: '.eyes.rightEye.skeleton2d',
                                                  Categories.SkeletonType.S3d: '.eye.rightEye.skeleton3d'
                                                  },
                                     'leftHand': {Categories.SkeletonType.S2d: '.hands.leftHand.skeleton2d',
                                                  Categories.SkeletonType.S3d: '.hands.leftHand.skeleton3d'
                                                  },
                                     'rightHand': {Categories.SkeletonType.S2d: '.hands.rightHand.skeleton2d',
                                                   Categories.SkeletonType.S3d: '.hands.rightHand.skeleton3d'
                                                   },
                                     'leftFoot': {Categories.SkeletonType.S2d: '.feet.leftFoot.skeleton2d',
                                                  Categories.SkeletonType.S3d: '.feet.leftFoot.skeleton3d'
                                                  },
                                     'rightFoot': {Categories.SkeletonType.S2d: '.feet.rightFoot.skeleton2d',
                                                   Categories.SkeletonType.S3d: '.feet.rightFoot.skeleton3d'
                                                   },
                                     }.get(x),
        }

        if type(annotation) == LandmarkLabel:
            if annotation.landmarkType is not None:
                return mapping[LandmarkLabel](label)[annotation.landmarkType]
            else:
                raise Exception('landmarkType must be set in order to distinguish between 2d or 3d landmark')
        elif type(annotation) == SkeletonEdge:
            if annotation.skeletonType is not None:
                return mapping[SkeletonEdge](label)[annotation.skeletonType]
            else:
                raise Exception('skeletonType must be set in order to distinguish between 2d or 3d skeleton')
        else:
            return mapping[type(annotation)](label)

    @staticmethod
    def _adjust_in_right_place(parent, path, sub_path, count, annotation):
        if count < len(path) - 1:
            sub_path = sub_path + '.' + path[count]
            intermediate = getattr(parent, path[count])
            if intermediate is None:
                intermediate = GlobalAnnotations._rev_structure[sub_path]()
                GlobalAnnotations._adjust_in_right_place(intermediate, path, sub_path, count + 1, annotation)
                setattr(parent, path[count], intermediate)
            else:
                GlobalAnnotations._adjust_in_right_place(intermediate, path, sub_path, count + 1, annotation)
        else:
            if type(annotation) == LandmarkLabel or type(annotation) == SkeletonEdge:
                getattr(parent, path[count]).add(annotation)
            else:
                setattr(parent, path[count], annotation)

    def add_annotation(self, annotation: BaseModel) -> None:
        """
        Add an annotation to the global annotations, annotation must be a subclass of base model.
        This method takes care of placing the annotation at the right place in global annotations automatically.

        :param annotation: The annotation to be added
        :return: None
        """
        self._generate_structure()
        if not isinstance(annotation, BaseModel):
            raise ValueError('Input annotation must be a sub-class of BaseModel')

        target_place = (GlobalAnnotations._structure.get(type(annotation)) or
                       GlobalAnnotations._structure.get(ValList[type(annotation)]))
        if target_place is None:
            raise ValueError('Annotation must belong to DTSubject, '
                             'check if you are adding the annotation at wrong place')

        if len(target_place) > 1:
            distinguisher = None
            if hasattr(annotation, 'label'):
                distinguisher = getattr(annotation, 'label')
            elif hasattr(annotation, 'region'):
                distinguisher = getattr(annotation, 'region')
            if distinguisher is None:
                raise ValueError('Ambiguous annotation provided, '
                                 'please set the "region" or "label" attribute to make it identifiable')
            if isinstance(distinguisher, Enum):
                target_place = GlobalAnnotations._distinguish_label_place(annotation, distinguisher.value)
            else:
                target_place = GlobalAnnotations._distinguish_label_place(annotation, distinguisher)
        else:
            target_place = target_place[0]

        target_place = target_place.strip('.').split('.')
        GlobalAnnotations._adjust_in_right_place(self, target_place, '', 0, annotation)
