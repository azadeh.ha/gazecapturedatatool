#!/usr/bin/env bash

source common

echo "Login to your gitlab docker registry to fetch the base image"

docker login registry.gitlab.com

image="$(getImageName)"

if [ -z "$image" ]; then
    echo "Name for docker image can not be determined,
    Please add git remote 'origin' before, so that image name can be resolved and image can be pushed to container registry"
    exit 1
fi

docker push "$image"
