from __future__ import annotations
from datatool_api.models.base.BaseDTDataset import BaseDTDataset
from datatool_api.data_validation.containers.StorageValDict import StorageValDict
from datatool_api.data_validation.containers.ValDict import ValDict
from custom_base_types import *
from typing import Tuple
import pandas


class DTDatasetCustom(BaseDTDataset):
    """
    Attributes:
        info: CocoDatasetInfo: Dataset Info
        licenses: ValDict: All licenses for the dataset with their respective categories
        categories: ValDict: All annotation categories for the dataset
        images: StorageValDict: Image info for all images in the dataset
        annotations: StorageValDict: Annotations for all images in the dataset
    """


    
    images: StorageValDict[str, GazeImageData] = Attribute(type=StorageValDict,element_constraints=Attribute(type=GazeImageData))
    motion: StorageValDict[str, GazeMotionData] = Attribute(type=StorageValDict,element_constraints=Attribute(type=GazeMotionData))
    info: StorageValDict[str, GazeDatasetInfo] = Attribute(type=StorageValDict,element_constraints=Attribute(type=GazeDatasetInfo))
															  

    def __init__(self, name: str, operatingMode: str):
        super().__init__(name, operatingMode)

    def __del__(self):
        super().__del__()

    """
    Needs to be implemented by the datatool creator
    """

    def to_pandas_frame(self, keep_original: bool = False, column_subset: List = None) -> List[Tuple[str,
                                                                                                     pandas.DataFrame]]:
        """
        Pandas dataframe generator for dataset

        :param keep_original: If original dataset needs to be kept in memory, if False, the original dataset object
        can be modified, by popping samples from it.
        :param column_subset: If only a subset of all columns are needed in the dataframe
        :return: List of (dataframe_name, pandas.Dataframe)
        """
        col_subset = {}
        if column_subset is not None and type(column_subset) == list:
            for s in column_subset:
                col_subset[s] = True
        output_dict = {}

        annotation_keys = list(self.images.keys())
        for key in annotation_keys:
            if keep_original is False:
                images = self.images.pop(key)
            else:
                images = self.images.get(key)
            flattened_annotation = {}
            images.flatten(flattened_annotation, 'images')

            # Export everything in flattened dict
            for k, v in flattened_annotation.items():
                if len(col_subset) == 0 or k in column_subset:
                    try:
                        output_dict[k].append(v)
                    except Exception as e:
                        output_dict[k] = [v]

        return [('images', pandas.DataFrame(output_dict))]

    def to_pandas_frame_for_report(self) -> List[Tuple[str, pandas.DataFrame, List[str]]]:
        """
        Get the pandas dataframe which is used for report generation. This method changes the column names to the
        expected names for columns in the report and only exports the columns which are required in the report.

        In addition it also export a column name list along with each dataframe holding the columns names which should
        be used to generate the interaction plots in the report.

        :return: List of (dataframe_name, pandas.Dataframe, List[columns_needed_for_interaction_plots])
        """
        # Columns to export in the data frame for reporting
        report_columns = {
            'images.image_ID': 'Image Id',
            'images.appleFace.isValid': 'Valid Face',
            'images.appleLeftEye.isValid': 'Valid Left Eye',
            'images.appleRightEye.isValid': 'Valid Right Eye'

        }

        report_interaction_columns = [
            'Valid Face',
            'Valid Left Eye',
            'Valid Right Eye'
        ]

        frame_out = self.to_pandas_frame(keep_original=False, column_subset=list(report_columns.keys()))
        out = []
        for tup in frame_out:
            tup[1].rename(columns=report_columns, inplace=True)
            out.append((tup[0], tup[1], report_interaction_columns))
        return out
