#!/usr/bin/env bash

set -e

CLONE_LINK=""
DATATOOL_DIR=""

function usage() {
  echo "Usage:

  source setup.sh

  Required Arguments:
    --datatool-repo-clone-link, -l        Clone link to the newly creted datatool repository
    --local-path, -p                      Local path where the datatool repository must be cloned and setup
  "
}

while (( $# )); do
  case "$1" in
    -h|--help)
      usage
      ;;
    -l|--datatool-repo-clone-link)
      CLONE_LINK="$2"
      shift 2
      ;;
    -p|--local-path)
      DATATOOL_DIR="$2"
      shift 2
      ;;
    -*|--*=)
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
  esac
done

if [ -z "$DATATOOL_DIR" ] || [ -z "$CLONE_LINK" ]; then
  usage
  exit 1
fi

echo "Cloning the datatool repository"
git clone "$CLONE_LINK" "$DATATOOL_DIR"

echo "Populating with template contents"
cp -r * "$DATATOOL_DIR"
cp .gitignore .dockerignore "$DATATOOL_DIR"

cd "$DATATOOL_DIR"
echo "Adding dependency submodules"
git submodule add https://gitlab.com/bonseyes/artifacts/data_tools/apis/datatool-api.git datatool_api
git submodule add https://gitlab.com/bonseyes/artifacts/data_tools/common/datatool-common-base.git common
git submodule update --init --recursive

rm setup.sh README.md
mv README_datatool.md README.md

git add .
git commit -m "First Commit with Contents from Template"

echo "Datatool setup done, please proceed and finish the datatool implementation,
please follow the datatool developer documentation [] for the next steps."