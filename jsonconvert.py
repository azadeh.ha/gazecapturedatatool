import json
import os
from sys import platform
import tarfile
import shutil
from tqdm import tqdm
def db_converter(path):
    
    dir_path = os.path.dirname(os.path.realpath(path))
    # Determine delimiter and webdriver address
    AddressDelimiter = '/'
       

    total_data = dict()
    total_data['images_data'] = list()
    total_data['motion'] = list()
    total_data['info'] = list()
    for inputFile in os.listdir(path):
        if '.tar.gz' in inputFile:
            print('find tar gz')
            folder_data = dict()
            structured_data = []
            file = tarfile.open(path+inputFile)
            file.extractall(f'.{AddressDelimiter}currentData')
            file.close()

            inputDataTemp = os.listdir(f'.{AddressDelimiter}currentData{AddressDelimiter}{inputFile.replace(".tar.gz", "")}')
            fileItems = dict()
            for jsonFile_temp in inputDataTemp:
                if '.json' in jsonFile_temp:
                    with open(f'.{AddressDelimiter}currentData{AddressDelimiter}{inputFile.replace(".tar.gz", "")}{AddressDelimiter}{jsonFile_temp}', 'r') as inFileTemp:
                        fileItems[jsonFile_temp.replace('.json', '')] = json.load(inFileTemp)
                   

            shutil.rmtree(f'.{AddressDelimiter}currentData{AddressDelimiter}{inputFile.replace(".tar.gz", "")}')

            for image_index in tqdm(range(fileItems['info']['TotalFrames']), desc=f'get image data from "{inputFile}" file.', ncols=100):
                image_data_temp = dict().fromkeys(list(filter(lambda X: X.lower() not in['info', 'motion'], fileItems.keys())), None)  # todo: bad setting
                # image_data_temp['frames'] = None
                for item_name, item_value in fileItems.items():

                    if item_name.lower() not in ['info', 'motion']:

                        if isinstance(item_value, dict):
                            for k in item_value.keys():
                                # check length
                                if len(item_value[k]) != fileItems['info']['TotalFrames']:
                                    raise Exception(f"data is not consistent! (length of {item_name}[{k}] [{len(item_value)}] is not equal to TotalFrames [{fileItems['info']['TotalFrames']}])")
                                if not image_data_temp[item_name]:
                                    image_data_temp[item_name] = dict()
                                image_data_temp[item_name][k] = item_value[k][image_index]
                        elif isinstance(item_value, list):
                            # check length
                            if len(item_value) != fileItems['info']['TotalFrames']:
                                raise Exception(f"data is not consistent! (length of {item_name} [{len(item_value)}] is not equal to TotalFrames [{fileItems['info']['TotalFrames']}])")

                            image_data_temp[item_name] = item_value[image_index]
                        else:
                            raise TypeError(f'Type of {item_name} content is not acceptable!')
                image_data_temp['image_ID'] = inputFile.replace(".tar.gz", "") + '_' + image_data_temp['frames'][:image_data_temp['frames'].index('.')]
                image_data_temp['subject_ID'] = inputFile.replace(".tar.gz", "")
                structured_data.append(image_data_temp)

            folder_data['images_data'] = structured_data
            folder_data['motion'] = fileItems['motion']
            folder_data['info'] = fileItems['info']
            

            total_data['images_data'].extend(folder_data['images_data'])
            
            folder_data_motion = []
            for item_index, item in enumerate(folder_data['motion']):
                item['subject_ID'] = inputFile.replace(".tar.gz", "")
                item['index'] = item_index
                folder_data_motion.append(item)

            total_data['motion'].extend(folder_data_motion)
            folder_data['info']['subject_ID'] = inputFile.replace(".tar.gz", "")
            total_data['info'].append(folder_data['info'])

    with open(f'structured_data.json', 'w', encoding='utf-8') as outFile:
        json.dump(total_data, outFile, indent=5)
        print('\nprocessing was completed')

    return total_data
