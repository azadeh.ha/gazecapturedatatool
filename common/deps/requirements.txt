jsonschema==3.2.0
PyYAML==5.4.1
boto3==1.17.53
tqdm==4.60.0
synology_api==0.1.3.3
pycryptodome==3.10.1
pandas==1.3.4
Pillow==8.4.0