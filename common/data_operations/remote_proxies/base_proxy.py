from abc import ABC, abstractmethod


class BaseProxy(ABC):
    @abstractmethod
    def upload_package(self, destination_dir_path: str, source_file_path: str) -> bool:
        pass

    @abstractmethod
    def download_package(self, destination_dir_path: str, source_object_path: str) -> str:
        pass

    @abstractmethod
    def check_if_exists(self, object_path: str) -> bool:
        pass
