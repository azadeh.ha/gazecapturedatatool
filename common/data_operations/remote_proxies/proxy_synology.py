from .base_proxy import BaseProxy
from synology_api import filestation
import os
from urllib import parse


class proxy_synology(BaseProxy):
    def __init__(self, address: str, root_path: str):
        address = address.split(':')
        if address[0] == 'http':
            self.protocol = 'http'
            self.address = address[1][2:]
            self.port = int(address[2])
        elif address[0] == 'https':
            self.protocol = 'https'
            self.address = address[1][2:]
            self.port = 443
        else:
            raise Exception('Wrongly formatted address provided, Accepted formats are '
                            'https://ADDRESS or http://IP:PORT')

        self.root_path = root_path
        if self.root_path[-1] != '/':
            self.root_path = self.root_path + '/'

        # Fetch the credentials from environment variables
        self.syno_creds = {'username': os.getenv('DATATOOL_USER'),
                           'password': os.getenv('DATATOOL_PASSWORD')}
        if (self.syno_creds['username'] is None or self.syno_creds['username'] == '' or
                self.syno_creds['password'] is None or self.syno_creds['password'] == ''):
            raise Exception('Environment variables "DATATOOL_USER" and "DATATOOL_PASSWORD" must be set')

        if self.protocol == 'https':
            self.file_station = filestation.FileStation(self.address, self.port, self.syno_creds['username'],
                                                        self.syno_creds['password'], secure=True, cert_verify=True)
        elif self.protocol == 'http':
            self.file_station = filestation.FileStation(self.address, self.port, self.syno_creds['username'],
                                                        self.syno_creds['password'], secure=False)

    def upload_package(self, destination_dir_path: str, source_file_path: str) -> bool:
        """
        Upload a file to synology

        :param destination_dir_path: Directory in synology where package should be uploaded
        :param source_file_path: Source file path on the machine
        :return: True on success, False on failure
        """
        try:
            print('Uploading the package')
            self.file_station.create_folder(self.root_path + os.path.dirname(destination_dir_path),
                                            name=os.path.basename(destination_dir_path))
            api_name = 'SYNO.FileStation.Upload'
            info = self.file_station.file_station_list[api_name]
            api_path = info['path']
            url = ('%s%s' % (self.file_station.base_url, api_path)) + '?api=%s&version=%s&method=upload&_sid=%s' % (
                api_name, info['minVersion'], self.file_station._sid)
            args = {
                    'path': self.root_path + destination_dir_path,
                    'create_parents': "true",
                    'overwrite': "true",
                    'file': '@' + os.path.realpath(source_file_path)
                    }

            command = 'curl -X POST'
            for key, value in args.items():
                command += ' -F '
                command += '"' + key + "=" + value + '"'
            command += ' "' + url + '" | cat'

            ret = os.system(command=command)
            print('')
            if ret != 0:
                print('Error in uploading the package')
                return False
            else:
                print('Upload Completed!')
                return True
        except Exception as e:
            print('Error in uploading the package')
            print(e)
            return False

    def download_package(self, destination_dir_path: str, source_object_path: str) -> str:
        """
        Download a file from synology with progress

        :param destination_dir_path: Directory in host where package should be downloaded
        :param source_object_path: Source object path on the synology
        :return: True on success, False on failure
        """
        try:
            print('Downloading the package')
            os.makedirs(destination_dir_path, exist_ok=True)
            api_name = 'SYNO.FileStation.Download'
            info = self.file_station.file_station_list[api_name]
            api_path = info['path']
            if source_object_path is None:
                return 'Enter a valid path'

            url = ('%s%s' % (self.file_station.base_url, api_path)) + '?api=%s&version=%s&method=download&path=%s&mode=%s&_sid=%s' % (
                api_name, info['maxVersion'], parse.quote_plus(os.path.join(self.root_path, source_object_path)), 'download', self.file_station._sid)
            command = 'curl -X GET -o ' + os.path.join(destination_dir_path, os.path.basename(source_object_path)) + \
                      ' "' + url + '"'

            ret = os.system(command=command)
            print('')
            if ret != 0:
                print('Error in Downloading the package')
                return ''
            else:
                print('Download Completed!')
                return os.path.join(destination_dir_path, os.path.basename(source_object_path))
        except Exception as e:
            print('Error in downloading the package')
            print(e)
            return ''

    def check_if_exists(self, object_path: str) -> bool:
        """
        Check if an object exists in synology under root path

        :param object_path: Object path relative to root path
        :return: bool
        """
        info = self.file_station.get_file_info(self.root_path + object_path)
        if 'isdir' in info['data']['files'][0]:
            return True
        else:
            return False
