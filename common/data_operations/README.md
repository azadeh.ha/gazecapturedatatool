## Data Operations
This package provides the common utilities used to pack and upload source dataset for a datatool.


### Packing
To pack a specific version of the original dataset into a zip archive for a datatool version:
```
python3 pack.py \
    --input-dir <INPUT_DIR> \
    --datatool-name <DATATOOL_NAME> \
    --datatool-version <DATATOOL_VERSION> \
    --datatool-tag <DATATOOL_TAG> \
    --output-dir <OUTPUT_DIR>
```

Required arguments:
```
    --input-dir, -i                 Input directory containing the datatool
    --datatool-name, -n              Name of the datatool
    --datatool-version, -v           Version of the datatool
    --datatool-tag, -t               Tag of the datatool 
    --output-dir, -o                Output directory where to place the output archive
```
This will zip the content of the input directory and put the output zip in output directory. Also `info.yml` and `info.txt` files will be created inside the output directory containing SHA256 checksum of the archive and size information for it.


### Uploading
To upload a specific version of packaged archive:
```
python3 upload.py \
    --archive-path <ARCHIVE_PATH> \
    --datatool-name <DATATOOL_NAME> \
    --datatool-version <DATATOOL_VERSION> \
    --datatool-tag <DATATOOL_TAG>
```

Required arguments:
```
    --archive-path, -p              Path to a packed archive created by using pack.py
    --datatool-name, -n              Name of the datatool
    --datatool-version, -v           Version of the datatool
    --datatool-tag, -t               Tag of the datatool 
```
This will upload the archive to the storage according to the configured storage in `../../config_files/storage_config.yml`.
