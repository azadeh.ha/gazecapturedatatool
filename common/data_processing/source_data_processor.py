from .source_data_reader import SourceDataReader
from .datatool_patch_interface import DatatoolPatchInterface
from .source_data_parser_interface import SourceDataParserInterface
import subprocess
import os
import inspect
import sys
from datetime import datetime
from .data_parsing_utils import DataParsingUtils
cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, cur_path + '/../../')
from datatool_api.models.DTDataset import DTDataset
from datatool_api.models.BaseTypes import DatasetMetadata
from custom_dataset_model import DTDatasetCustom


class DataProcessor:
    def __init__(self, datatool_name: str,
                 input_path: str,
                 output_dir: str,
                 datatool_version: str,
                 datatool_tag: str,
                 source_data_parser: SourceDataParserInterface,
                 datatool_patch_creator: DatatoolPatchInterface,
                 operation_mode: str = 'memory',
                 validate_sample_paths: bool = True,
                 **kwargs
                 ):
        """
        Initialize Data Processor

        :param datatool_name: Name of the data tool
        :param input_path: Input path to the raw data, can be a path to a directory or a zip file
        :param output_dir: Output directory to dump the datatool output
        :param datatool_version: Version for the datatool to run
        :param datatool_tag: Tag for the datatool to run
        :param source_data_parser: Data parser instance
        :param datatool_patch_creator: Datatool patch creator instance
        :param operation_mode: Datatool API operation mode
        :param validate_sample_paths: If sample paths to be validated while dumping the dataset.json
        :param kwargs: Additional keyword arguments which might be needed for patch running:
        """
        self.datatool_name = datatool_name
        self.source_path = input_path
        self.output_dir = os.path.join(output_dir, datatool_tag)
        self.datatool_version = datatool_version
        self.datatool_tag = datatool_tag
        self.operation_mode = operation_mode
        self.validate_sample_paths = validate_sample_paths
        self.kwargs = kwargs
        self.data_reader = SourceDataReader(self.source_path)
        self.data_parser = source_data_parser
        self.datatool_patcher = datatool_patch_creator
        os.makedirs(self.output_dir, exist_ok=True)
        DataParsingUtils.output_dir = self.output_dir

        try:
            self.dataset = DTDatasetCustom(name=self.datatool_name, operatingMode=self.operation_mode)
            self.customDataModel = True
        except Exception as e:
            self.dataset = DTDataset(name=self.datatool_name, operatingMode=self.operation_mode)
            self.customDataModel = False

    @staticmethod
    def get_current_git_hash():
        hash_file_created = False
        try:
            ret = subprocess.run('git log -1 | grep commit > git_hash.txt', check=True, shell=True, capture_output=True)
            if ret.returncode == 0:
                hash_file_created = True
        except Exception as e:
            pass

        try:
            with open('git_hash.txt', 'r') as r:
                for line in r.readlines():
                    line = line.strip('\n').split(' ')[1]
                    if hash_file_created is True:
                        os.remove('git_hash.txt')
                    return line
        except Exception as e:
            print(e)
            return ''

    def run(self):
        metadata = DatasetMetadata(datatoolName=self.datatool_name, datatoolVersion=self.datatool_version,
                                   datatoolTag=self.datatool_tag, createdBy='datatool.py',
                                   creationTime=datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'),
                                   gitHash=self.get_current_git_hash()
                                   )
        self.dataset.metadata = metadata

        if self.customDataModel is True:
            self.data_parser.extract(self.source_path, self.dataset, self.datatool_version, self.datatool_tag,
                                     self.output_dir)
        else:
            for name, ext in self.data_reader.files():
                self.data_parser.parse_file(self.data_reader, name, ext, self.dataset, self.datatool_version,
                                            self.datatool_tag)
            self.data_parser.post_process(self.dataset, self.datatool_version, self.datatool_tag)

        self.dataset.to_json(os.path.join(self.output_dir, 'dataset.json'),
                             validate_sample_paths=self.validate_sample_paths)
        self.dataset.to_json(os.path.join(self.output_dir, 'dataset_sample.json'),
                             validate_sample_paths=self.validate_sample_paths,
                             example_mode=True)

        self.datatool_patcher.run_patch(self.dataset, self.operation_mode,
                                        self.validate_sample_paths, self.output_dir, **self.kwargs)
