from .base.BaseSample import BaseSample
from ..data_validation.attribute import Attribute
from .BaseTypes import AudioSampleMetadata, AudioKeyword
from ..data_validation.containers.ValDict import ValDict


class AudioSample(BaseSample):
    transcription: str = Attribute(type=str, ne='')
    keywords: ValDict[str, AudioKeyword] = Attribute(type=ValDict,
                                                     min_items=1,
                                                     element_constraints=Attribute(type=AudioKeyword, rsa=['label'])
                                                     )
    metadata: AudioSampleMetadata = Attribute(type=AudioSampleMetadata, msa=1)

    def __init__(self, id: str = None, samplePath: str = None, transcription: str = None,
                 keywords: ValDict[str, AudioKeyword] = None, metadata: AudioSampleMetadata = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)
