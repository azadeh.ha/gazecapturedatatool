from .BaseTypes import *
from enum import Enum


class Subject(BaseModel):
    id: str = Attribute(type=str, ne='')
    occluded: bool = Attribute(type=bool)
    actionUnits: ActionUnits = Attribute(type=ActionUnits, msa=1)
    emotions: Emotions = Attribute(type=Emotions, msa=1)
    age: Age = Attribute(type=Age, osa=['years', 'group'])
    gender: Gender = Attribute(type=Gender, rsa=['label'])
    ethnicity: Ethnicity = Attribute(type=Ethnicity, rsa=['label'])
    nationality: Nationality = Attribute(type=Nationality, rsa=['label'])
    face: Face = Attribute(type=Face, msa=1)
    headpose: Headpose = Attribute(type=Headpose, rsa=['yaw', 'pitch', 'roll'])
    eyes: Eyes = Attribute(type=Eyes, msa=1)
    eyeGaze: EyeGaze = Attribute(type=EyeGaze, msa=1)
    pain: Pain = Attribute(type=Pain, msa=1)
    transients: Transients = Attribute(type=Transients, msa=1)
    body: Body = Attribute(type=Body, msa=1)
    hands: Hands = Attribute(type=Hands, msa=1)
    feet: Feet = Attribute(type=Feet, msa=1)
    activity: Activity = Attribute(type=Attribute, msa=1)
    scaleFactor: ScaleFactor = Attribute(type=ScaleFactor, rsa=['value'])
    crowded: bool = Attribute(type=bool)

    def __init__(self, id: str = None, occluded: bool = None, actionUnits: ActionUnits = None,
                 emotions: Emotions = None, age: Age = None, gender: Gender = None, ethnicity: Ethnicity = None,
                 nationality: Nationality = None, face: Face = None, headpose: Headpose = None, eyes: Eyes = None,
                 eyeGaze: EyeGaze = None, pain: Pain = None, transients: Transients = None, body: Body = None,
                 hands: Hands = None, feet: Feet = None, activity: Activity = None, scaleFactor: ScaleFactor = None,
                 crowded: bool = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    @staticmethod
    def _distinguish_label_place(annot_type, label):
        mapping = {
            ActionUnit: lambda x: '.actionUnits.{}'.format(x),
            Emotion: lambda x: '.emotions.{}'.format(x),
            BoundingBox: lambda x: {'face': '.face.boundingBox',
                                    'body': '.body.boundingBox',
                                    'leftEye': '.eyes.leftEye.boundingBox',
                                    'rightEye': '.eyes.rightEye.boundingBox',
                                    'leftHand': '.hands.leftHand.boundingBox',
                                    'rightHand': '.hands.rightHand.boundingBox',
                                    'leftFoot': '.feet.leftFoot.boundingBox',
                                    'rightFoot': '.feet.rightFoot.boundingBox'
                                    }.get(x),
            Cuboid: lambda x: {'face': '.face.cuboid',
                               'body': '.body.cuboid',
                               'leftEye': '.eyes.leftEye.cuboid',
                               'rightEye': '.eyes.rightEye.cuboid',
                               'leftHand': '.hands.leftHand.cuboid',
                               'rightHand': '.hands.rightHand.cuboid',
                               'leftFoot': '.feet.leftFoot.cuboid',
                               'rightFoot': '.feet.rightFoot.cuboid'
                               }.get(x),
            Landmark2D: lambda x: {'face': '.face.landmarks2d',
                                   'body': '.body.landmarks2d',
                                   'leftEye': '.eyes.leftEye.landmarks2d',
                                   'rightEye': '.eyes.rightEye.landmarks2d',
                                   'leftHand': '.hands.leftHand.landmarks2d',
                                   'rightHand': '.hands.rightHand.landmarks2d',
                                   'leftFoot': '.feet.leftFoot.landmarks2d',
                                   'rightFoot': '.feet.rightFoot.landmarks2d'
                                   }.get(x),
            Landmark3D: lambda x: {'face': '.face.landmarks3d',
                                   'body': '.body.landmarks3d',
                                   'leftEye': '.eyes.leftEye.landmarks3d',
                                   'rightEye': '.eyes.rightEye.landmarks3d',
                                   'leftHand': '.hands.leftHand.landmarks3d',
                                   'rightHand': '.hands.rightHand.landmarks3d',
                                   'leftFoot': '.feet.leftFoot.landmarks3d',
                                   'rightFoot': '.feet.rightFoot.landmarks3d'
                                   }.get(x),
            Segmentation: lambda x: {'face': '.face.segmentation',
                                     'body': '.body.segmentation',
                                     'leftEye': '.eyes.leftEye.segmentation',
                                     'rightEye': '.eyes.rightEye.segmentation',
                                     'leftHand': '.hands.leftHand.segmentation',
                                     'rightHand': '.hands.rightHand.segmentation',
                                     'leftFoot': '.feet.leftFoot.segmentation',
                                     'rightFoot': '.feet.rightFoot.segmentation'
                                     }.get(x),
            Eye: lambda x: '.eyes.{}'.format(x),
            Gaze: lambda x: '.eyeGaze.{}'.format(x),
            Hand: lambda x: '.hands.{}'.format(x),
            Foot: lambda x: '.feet.{}'.format(x)
        }

        return mapping[annot_type](label)

    @staticmethod
    def _adjust_in_right_place(parent, path, sub_path, count, annotation):
        if count < len(path) - 1:
            sub_path = sub_path + '.' + path[count]
            intermediate = getattr(parent, path[count])
            if intermediate is None:
                intermediate = Subject._rev_structure[sub_path]()
                Subject._adjust_in_right_place(intermediate, path, sub_path, count + 1, annotation)
                setattr(parent, path[count], intermediate)
            else:
                Subject._adjust_in_right_place(intermediate, path, sub_path, count + 1, annotation)
        else:
            if type(annotation) == Landmark2D or type(annotation) == Landmark3D:
                getattr(parent, path[count]).add(annotation)
            else:
                setattr(parent, path[count], annotation)

    def add_annotation(self, annotation: BaseModel) -> None:
        """
        Add an annotation to the subject, annotation must be a subclass of base model.
        This method takes care of placing the annotation at the right place in subject automatically.

        :param annotation: The annotation to be added
        :return: None
        """
        self._generate_structure()
        if not isinstance(annotation, BaseModel):
            raise ValueError('Input annotation must be a sub-class of BaseModel')

        target_place = Subject._structure.get(type(annotation)) or Subject._structure.get(ValList[type(annotation)])
        if target_place is None:
            raise ValueError('Annotation must belong to DTSubject, '
                             'check if you are adding the annotation at wrong place')

        if len(target_place) > 1:
            distinguisher = None
            if hasattr(annotation, 'label'):
                distinguisher = getattr(annotation, 'label')
            elif hasattr(annotation, 'region'):
                distinguisher = getattr(annotation, 'region')
            if distinguisher is None:
                raise ValueError('Ambiguous annotation provided, '
                                 'please set the "region" or "label" attribute to make it identifiable')
            if isinstance(distinguisher, Enum):
                target_place = Subject._distinguish_label_place(type(annotation), distinguisher.value)
            else:
                target_place = Subject._distinguish_label_place(type(annotation), distinguisher)
        else:
            target_place = target_place[0]

        target_place = target_place.strip('.').split('.')
        Subject._adjust_in_right_place(self, target_place, '', 0, annotation)
