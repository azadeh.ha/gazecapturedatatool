# NV Data Validation

This api enables to write data models classes with data validation capabilities. 
Any data model class inheriting the `BaseModel` from this api can define attributes with constraints. The api ensures all the constraints are satisfied for data model class instances when data is inserted or changed for the instance. 

## Examples

```
from base_model import BaseModel
from attribute import Attriute

class Emotion(BaseModel):
    activated: bool = Attribute(type=bool)
    arousal: float = Attribute(type=float, ge=0.0, le=1.0)
    valance: float = Attribute(type=float, ge=-1.0, le=1.0)
    confidence: float = Attribute(type=float, ge=0.0, le=1.0)
```

In the example above, we have defined a data model for Emotion which has 4 attributes i.e.  
- activated, which is of type bool   
- arousal, which is of type float and ranges between 0.0 and 1.0  
- valance, which is of type float and ranges between -1.0 and 1.0  
- confidence, which is type float and ranges between 0.0 and 1.0

```
emo = Emotion(activated=True) # Creates an emotion instance with activated value as True
emo.activated = 1       # Raises exception as attribute is of type bool
emo.activated = False   # Correct
emo.arousal = 1         # Raises exception as attribute is of type float
emo.arousal = 1.0       # Correct
emo.valance = -1.5      # Raises an exception as minimum value is defined as -1.0 via "ge (greater than equal to)"   
```

By default, all attributes are None if they are not set by the user. User can specify an attribute value either via the constructor or by using the setter property. 

List of all available constraints can be found in `DTDataValidator.validators` from [`validator.py`](/validator.py).   
It is possible to define and add more constraints by using `DTDataValidator.add_validator()` method.

```
from base_model import BaseModel
from attribute import Attriute

class EmotionList(BaseModel):
    emoList:List[Emotion] = Attribute(type=list, min_items=1, elements=Emotion, rsa=['activated'])
```

In the example above, we have defined a data model for EmotionList which has 1 attribute i.e.  
- emoList, which is of type list with at least one item inside, with elements of type "Emotion" and each element must have at least "activated" attribute set.

```
emolist = EmotionList()
emolist.emoList = []                            # Raises exception as min items must be 1
emolist.emoList = [1.0]                         # Raised exception as elements of list must of type Emotion
emolist.emoList = [Emotion()]                   # Raises exception as all elements must have "activated" attribute set
emolist.emoList = [Emotion(activated=True)]     # Correct 
```

The api achieves the data validation by automatically creating the "getters" and "setters" for the class attributes and applying the data validation based on constraints in the setter.
However, sometimes it's desirable to write the custom getters and setters for the class attributes.  

It is possible to prevent the api from creating the getter and setters automatically by adding an '_'
(underscore) in the name of the attribute. This tells the api that user will take care of defining the getters and setters for this attribute and its validation. This type of behaviour is desirable when dealing with attributes of type list or dict. Below is an example which handles a list.

```
from base_model import BaseModel
from attribute import Attriute
from validator import validate

class EmotionList(BaseModel):
    _emoList:List[Emotion] = Attribute(type=list, min_items=1, elements=Emotion, rsa=['activated'])
    
    @property
    def emoList(self):
        if self._emoList is not None:
            for val in self._emoList:
                yielf val 
        
    @emoList.setter
    @validate(type=list, min_items=1, elements=Emotion, rsa=['activated'])
    def emoList(self, value):
        self._emoList = value
```

In the example above, the "_" in front of "emoList", tells the api that user will take care of defining the getters and setters for it. The getter instead of returning the list, returns a generator over the list which prevents the user from accidentally modifying the list from getter value.

